﻿
namespace FinancesInstruments
{
    public static class StringUtils
    {
        /// <summary>
        /// Сжатие (выбрасывание пробелов из строки класса String)
        /// </summary>
        /// <param name="s">строка</param>
        /// <returns></returns>
        public static string СontractionSpace(this string s)
        {
            return s.Replace(" ","");
        }
    }
}
