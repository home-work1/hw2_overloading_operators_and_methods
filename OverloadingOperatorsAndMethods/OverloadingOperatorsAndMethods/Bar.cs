﻿using System;

namespace FinancesInstruments
{
    /// <summary>
    /// класс хранит БАР, это дата за которую содержится информация о ценнах в эту дату для финансового инструмента
    /// </summary>
    public class Bar
    {
        /// <summary>
        /// дата БАРа
        /// </summary>
        public DateTime DateBar { get; set; }
        /// <summary>
        /// начальная цена, цена первой сделки по финансовому инструменту
        /// </summary>
        public double OpenPrice { get; set; }
        /// <summary>
        /// цена закрытия, цена последней сделки по финансовому инструменту
        /// </summary>
        public double ClosePrice { get; set; }
        /// <summary>
        /// минимальная цена сделки за период
        /// </summary>
        public double MinPrice { get; set; }
        /// <summary>
        /// максимальная цена сделки за период
        /// </summary>
        public double MaxPrice { get; set; }

        /// <summary>
        /// переопределение метода ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"< {DateBar.ToString()}, OpP:{OpenPrice}, ClP: {ClosePrice}, "
                        + $" MinP: {MinPrice}, MaxP: {MaxPrice} >";
        }
    }
}
