﻿using FinancesInstruments;
using System;

namespace OverloadingOperatorsAndMethods
{
    //программа для тестирования
    class Program
    {
        static void Main(string[] args)
        {
            //конструктор
            HistoryBar histBar = new HistoryBar();

            //методы
            histBar.AddBar(
                new Bar() { DateBar = DateTime.Parse("07-01-2020"),
                            OpenPrice = 120,
                            ClosePrice = 110,
                            MaxPrice = 125,
                            MinPrice = 90}
                );

            //оператор +
            histBar += new Bar()
            {
                DateBar = DateTime.Parse("08-01-2020"),
                OpenPrice = 125,
                ClosePrice = 130,
                MaxPrice = 145,
                MinPrice = 70
            };

            //индексатор
            Console.WriteLine(histBar[0]);
            Console.WriteLine(histBar);

            //методы
            histBar.DeleteBar(DateTime.Parse("07-01-2020"));
            Console.WriteLine(histBar);

            HistoryBar histBar2 = new HistoryBar();
            histBar2.AddBar(
                    new Bar()
                    {
                        DateBar = DateTime.Parse("09-01-2020"),
                        OpenPrice = 150,
                        ClosePrice = 130,
                        MaxPrice = 145,
                        MinPrice = 20
                    });

            //перегрузка оператора +
            HistoryBar histBar3 = histBar + histBar2;

            Console.WriteLine(histBar3);

            //метод расширения для класса string
            Console.WriteLine(histBar3.ToString().СontractionSpace());

            //итератор
            foreach (Bar b in histBar)
                Console.WriteLine(b);

            Console.ReadKey();

        }
    }
}
