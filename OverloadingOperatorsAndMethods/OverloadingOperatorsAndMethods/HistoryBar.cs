﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace FinancesInstruments
{
    /// <summary>
    /// хранение истории БАРов
    /// </summary>
    public class HistoryBar : IEnumerable
    {
        private List<Bar> _listBar;

        /// <summary>
        /// конструктор
        /// </summary>
        public HistoryBar()
        {
            _listBar = new List<Bar>();
        }

        /// <summary>
        /// Добавление БАРа
        /// </summary>
        /// <param name="bar">БАР</param>
        public void AddBar(Bar bar)
        {
            _listBar.Add(bar);
        }

        /// <summary>
        /// Удаление БАРА
        /// </summary>
        /// <param name="dateTimeBar">время за которое БАРы будут удалены </param>
        /// <returns></returns>
        public int DeleteBar(DateTime dateTimeBar)
        {
            int countDeleted = _listBar.RemoveAll((a) => a.DateBar == dateTimeBar);
            return countDeleted;
        }

        /// <summary>
        /// переопределение метода ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "{" + string.Join(",", _listBar) + "}";
        }

        /// <summary>
        /// индексатор
        /// </summary>
        /// <param name="i">индекс</param>
        /// <returns></returns>
        public Bar this[int i]
        {
            get
            {
                return _listBar[i];
            }
        }

        /// <summary>
        /// Енумератор
        /// </summary>
        /// <returns>Возвращает экземпляры Bar</returns>
        public IEnumerator GetEnumerator()
        {
            foreach (Bar b in _listBar)
            {
                yield return b;
            }
        }

        /// <summary>
        /// добавляет к содержимому первого слагаемого экземпляра HistoryBar экземляр Bar, и возвращает итоговый экземпляр HistoryBar
        /// </summary>
        /// <param name="histBar">HistBar</param>
        /// <param name="bar">Bar</param>
        /// <returns></returns>
        public static HistoryBar operator +(HistoryBar histBar,Bar bar)
        {
            HistoryBar hb = (HistoryBar)histBar.MemberwiseClone();
            hb.AddBar(bar);
            return hb;
        }

        /// <summary>
        /// добавляет к содержимому первому слагаемому экземпляра HistoryBar, содержимое второго слагаемого экземпляра HistoryBar и возвращает первое слагаемое
        /// </summary>
        /// <param name="histBar1"></param>
        /// <param name="histBar2"></param>
        /// <returns></returns>
        public static HistoryBar operator +(HistoryBar histBar1, HistoryBar histBar2)
        {
            HistoryBar hb = (HistoryBar)histBar1.MemberwiseClone();

            foreach (Bar b in histBar2)
            {
                hb.AddBar(b);
            }

            return histBar1;
        }


    }
}
